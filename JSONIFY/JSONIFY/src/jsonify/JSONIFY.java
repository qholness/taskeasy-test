/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsonify;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.lang.ArrayIndexOutOfBoundsException;
/**
 *
 * @author homepc
 */
public class JSONIFY{
    private static FileReader GetFileIn(String path) {
        // Initialize file input object
        FileReader reader = null;
        try {
            reader = new FileReader(path);
        } catch (FileNotFoundException fnfExcept) {
            System.out.println("File Not Found Exception thrown: " + fnfExcept.getMessage());
            System.exit(42); // Break the service, no file means failing updates
        }
        return reader;
    }
    private static FileWriter GetFileOut(String path) {
        // Initialize file output object
        FileWriter writer = null;
        try {
            writer = new FileWriter(path);
        } catch (IOException ioe) {
            System.out.println("IO Exception thrown: " + ioe.getMessage());
            System.exit(42); // Break the service, no file means failing updates
        }
        return writer;
    }
    
    private static String[] GetHeader(BufferedReader r, String split) {
        // Get header of CSV file and return as a String[] object
        String[] header = null;
        try {
            String head = r.readLine();
            header = head.split(split); // Read the first line of the csv as the header
            return header;
        } catch (IOException ioe) {
            System.out.println("IO Exception thrown: " + ioe.getMessage());
            System.exit(42); // Break the service, no header means invalid JSON array.
        }
        return header;   
    }
    
    public static String GenJSON(String inpath, String outpath, String delimeter) {
        FileReader JimmyMcNulty = GetFileIn(inpath);
        FileWriter LesterFreamon = GetFileOut(outpath);
        
        // Init buffered reader/writer objects
        BufferedReader Reader = new BufferedReader(JimmyMcNulty);
        BufferedWriter OutFile = new BufferedWriter(LesterFreamon);
        
        // Build JSON Array
        String JSON = "["; // Start JSON array
        String line; // Line parser
        String[] data; // List of strings to host data
        String[] header = GetHeader(Reader, delimeter);
        Integer headerLength = header.length;
        
        try {
            // Go through file, split lines by `csvSplit` and write to a JSON string
            while((line = Reader.readLine()) != null) {
                data = line.split(delimeter);
                String out = "{"; // Init JSON map
                for(int i=0; i < headerLength; i++) {
                    // Craft JSON entry
                    out += header[i] + ":" + data[i]; // What if it reads errant data?
                    out += (i == headerLength - 1) ? "" : ","; // Add comma where necessary
                }
                JSON += out + "},"; // Close JSON map
                JSON += "\n"; // Optional... But... Meh...
            }
            JSON = JSON.substring(0, JSON.length() - 2); // Remove trailing comma and ending \n character
            JSON += "]"; // Close JSON array
            OutFile.write(JSON); // Write JSON file to outfile
            OutFile.close(); // Close to ensure update
        } catch (IOException ioe) {
            // Something went wrong with the call...
            System.out.println("IO Exception thrown: " + ioe.getMessage());
        }
        return JSON;
    }
    public static void main(String[] args) {
        String infile;
        String outfile;
        String delimeter;
        try {
            // Accept first two args as input and output file
            infile = args[0];
            outfile = args[0].replaceAll(".csv", ".json");
            delimeter = args[1];
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            // Revert to defaults
            infile = "../../../TaskEasy/data/academy_award_actresses.csv";
            outfile = infile.replaceAll(".csv", ".json");
            delimeter = ",";
        }
        
        try{
            // Setup a persistent server that return the JSON
            ServerSocket server = new ServerSocket(5101);
            while(true){
                Socket client = server.accept();
                String json = GenJSON(infile, outfile, delimeter);
                System.out.println("Updating: " + outfile);
            }            
        } catch (IOException ioe) {
            System.out.println("IO Exception thrown: " + ioe.getMessage());
        } 
    }    
}
