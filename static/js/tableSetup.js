function GenerateHeader() {
    // Generate header row for table
    var Hrow = $("<thead>"),
        htd = $("<td>").html("Academy Award Winning Actresses");
    Hrow.append(htd);
    return Hrow;
}

function GenerateRowData(e) {
    // Generate row data from an element object
    var row = $("<tr>"),
        td = $("<td>").html(e.Year);
    row.append(td);
    row.on("click", function(){
        alert("Year: " + e.Year + "\n" 
            + "Actress: " + e.Actress + "\n" 
            + "Movie: " + e.Movie);
    })
    return row;
}

function GenTable(request, tableObject) {
    // Push JSON data to tableObject (preferrably a table body)
    request.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            var JSONData = JSON.parse(this.responseText);
            JSONData.forEach(element => {
                tableObject.append(GenerateRowData(element));
            });
        }
    };
};

function ClearTable(tableObject) {
    tableObject.empty();
}