function Request(url, method) {
    // Request data from generated JSON file
    request = new XMLHttpRequest();
    request.open(method, url, true);
    request.send();
    return request;
}