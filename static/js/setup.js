$(window).on("load", function() {
    var button = $("#CLICK"),
        Table = $("#DataTable"),
        tbody = $("#tbody");
    button.on("click", function() {
        // Request to Java server to update data
        var update = Request("http://localhost:5101", "GET");
        var request = Request("data/academy_award_actresses.json", "GET");
        ClearTable(tbody);
        GenTable(request, tbody);
        button.html("Refresh Table"); // Update button text
    });
});